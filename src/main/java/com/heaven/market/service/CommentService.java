package com.heaven.market.service;

import com.heaven.market.model.Comment;
import com.heaven.market.model.Product;
import com.heaven.market.repository.CommentRepository;
import com.heaven.market.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by son on 25/04/2016.
 */
@Service
public class CommentService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CommentRepository commentRepository;

    public Comment addComment(Integer productID, Comment comment) {
        Product product = productRepository.findOne(productID);
        product.getCommentList().add(comment);
        return commentRepository.save(comment);
    }

    public boolean deleteComment(Integer productID, Integer commentID) {
        Product product = productRepository.findOne(productID);
        product.getCommentList().remove(commentRepository.findOne(commentID));
        commentRepository.delete(commentID);
        return true;
    }

    public boolean editComment(Integer productID, Comment newComment) {
        Product product = productRepository.findOne(productID);
        Comment oldComment = commentRepository.findOne(newComment.getCommentID());

        oldComment.setContentCmt(newComment.getContentCmt());
        product.getCommentList().remove(oldComment);
        product.getCommentList().add(newComment);
        return true;
    }
}
