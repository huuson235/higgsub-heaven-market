package com.heaven.market.service;

import com.heaven.market.model.InvertedIndex;
import com.heaven.market.model.Product;
import com.heaven.market.model.Register;
import com.heaven.market.model.Student;
import com.heaven.market.repository.InvertedIndexRepository;
import com.heaven.market.repository.ProductRepository;
import com.heaven.market.repository.RegisterRepository;
import com.heaven.market.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Storm on 23/04/2016.
 */
@Service
public class HeavenMarketService {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private RegisterRepository registerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private InvertedIndexRepository invertedIndexRepository;

    /*
    * Code of Son
    * */
    // remove mark of Vietnamese word.
    private static String unAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").toLowerCase().replaceAll("đ", "d");
    }

    // Xu ly dau cach' trong string
    private static String fixSpacesString(String s) {
        return s.trim().replaceAll("\\s+", " ");
    }

    /*
        Lớp Student
     */
    public List<Student> getStudents(){
        List<Student> students = (List<Student>) studentRepository.findAll();
        return students;

    }

    public Student createStudent(Student student){
        return studentRepository.save(student);
    }

    public Student findStudentByToken(String token){
        return studentRepository.findOne(token);
    }

    public Student findStudentByUsername(String username){
        List<Student> allStudents = (List<Student>)studentRepository.findAll();
        for (Student student:allStudents){
            if (student.getUsername().equals(username)){
                return student;
            }
        }
        return null;
    }

    public boolean deleteStudent(String username){
        Student student = findStudentByUsername(username);
        if( student != null){
            studentRepository.delete(student);
            return true;
        }
        return false;
    }

    public boolean isStudent(String token){
        if(findStudentByToken(token) != null)
            return true;
        else
            return false;
    }

    public boolean changePasswordStudent(String username, String newPassword){
        findStudentByUsername(username).setPassword(newPassword);
        return true;
    }
    /*
        Lớp Register
     */

    public String getExpiredDate(String token){
        return findStudentByToken(token).getExpiredDate();
    }

    public boolean updateStudent(String oldToken, String newToken, String expiredDate){
        Student student = findStudentByToken(oldToken);
        student.setToken(newToken);
        student.setExpiredDate(expiredDate);
        return true;
    }

    public Register createRegister(Register register){
        return registerRepository.save(register);
    }

    public Register findRegisterByToken(String token){
        return registerRepository.findOne(token);
    }

    public Register findRegisterByUsername(String username){
        List<Register> allRegisters = (List<Register>)registerRepository.findAll();
        for (Register register:allRegisters){
            if (register.getUsername().equals(username)){
                return register;
            }
        }
        return null;
    }

    public boolean deleteRegister(String username){
        Register register = findRegisterByUsername(username);
        if( register != null){
            registerRepository.delete(register);
            return true;
        }
        return false;
    }

    public boolean isRegister(String token){
        if(findRegisterByToken(token) != null)
            return true;
        else
            return false;
    }

    public boolean changePasswordRegister(String username, String newPassword){
        findRegisterByUsername(username).setPassword(newPassword);
        return true;
    }

    /*
        Lớp Product - Son add some method
     */

    public String getExpiredDateR(String token){
        return findRegisterByToken(token).getExpiredDate();
    }

    public boolean updateRegister(String oldToken, String newToken, String expiredDate){
        Register register = findRegisterByToken(oldToken);
        register.setToken(newToken);
        register.setExpiredDate(expiredDate);
        return true;
    }

    public Product createProduct(String username, Product product){
        // bo dau tieng viet
        product.setNameProductENG(fixSpacesString(unAccent(product.getNameProductVN())));
        // them word cho Inverted Index
        createInvertedIndex(product);

        findStudentByUsername(username).getProductList().add(product);
        return productRepository.save(product);
    }

    //Tìm kiếm đối với Student để quản lí sản phẩm của mình
    public Product findProductByStudent(String username, int uidProduct){
        List<Product> productList = findRegisterByUsername(username).getProductList();
        for (Product product:productList){
            if(product.getProductId() == uidProduct){
                return product;
            }
        }
        return null;
    }

    public boolean updateProduct(String username, int uidProduct, String content, String price){
        Product product = findProductByStudent(username, uidProduct);
        product.setContent(content);
        product.setPriceProduct(price);
        return true;
    }

    //Người dùng tìm kiếm sản phẩm
    public List<Product> searchProduct(String nameProductENG){
        List<Student> students = (List<Student>) studentRepository.findAll();
        List<Product> productList = new ArrayList<Product>();

        for (Student student:students){
            List<Product> allProduct = (List<Product>)student.getProductList();
            for(Product product:allProduct){
                //Tìm theo tên sản phẩm
            /*
            if(product.getNameProductENG().equals(nameProductENG)){
                productList.add(product);
            }
            */
                if(checkSearchProduct(product.getNameProductENG(), nameProductENG)){
                    productList.add(product);
                }
                //Tìm theo loại sản phẩm
            /*
            if(product.getTypeProduct().equals(nameProductENG)){
                productList.add(product);
            }
            */
                if(checkSearchProduct(product.getTypeProduct(), nameProductENG)){
                    productList.add(product);
                }
            }
        }

        return productList;
    }

    //Kiểm tra 1 chuỗi có phải là con của chuỗi kia không ?
    private boolean checkSearchProduct(String parent, String children){
        int count = 0;
        String [] arrParent = parent.split(" ");
        String [] arrChildren = children.split(" ");
        for (int i = 0; i < arrChildren.length; i++){
            for (int j = 0; j < arrParent.length; j++){
                if (arrChildren[i].equals(arrParent[j])){
                    count ++;
                    break;
                }
            }
        }

        if(count == arrChildren.length)
            return true;
        return false;
    }

    // Tao them du lieu cho InvertedIndex
    public void createInvertedIndex(Product product) {
        // Danh sach cac word trich xuat tu NameUnaccentProduct (ten sau khi bo dau)
        String[] wordList = product.getNameProductENG().split(" ");

        for (String word : wordList) {
            // key = word tuong ung trong inverted Index databse
            InvertedIndex key = invertedIndexRepository.findOne(word);
            if (key == null) { // key chua xuat hien trong database (new key)
                InvertedIndex newWord = new InvertedIndex(word, product.getProductId());
                invertedIndexRepository.save(newWord);
            } else {
                key.addSequence(product.getProductId());
            }
        }
    }
}
