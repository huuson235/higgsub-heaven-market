package com.heaven.market.service;

import com.heaven.market.model.InvertedIndex;
import com.heaven.market.model.Product;
import com.heaven.market.repository.InvertedIndexRepository;
import com.heaven.market.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by son on 25/04/2016.
 */
@Service
public class SearchService {
    @Autowired
    InvertedIndexRepository invertedIndexRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    HeavenMarketService heavenMarketService;

    // update bang Inverted Index
    private void updateInverted() {
        List<Product> products = (List<Product>) productRepository.findAll();
        for (Product product : products) {
            heavenMarketService.createInvertedIndex(product);
        }
    }
    // Ham chuyen tu String "1,2,3,5,6," => {1, 2, 3, 5, 6}
    private static List<Integer> convertStringToIntList(String s) {
        List<Integer> intList = new ArrayList<Integer>();
        s = s.substring(0, s.length() - 1);
        String[] words = s.split(",");
        for (String word : words) {
            intList.add(Integer.parseInt(word));
        }
        return intList;
    }

    // remove mark of Vietnamese word.
    private static String unAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").toLowerCase().replaceAll("đ", "d");
    }

    // Xu ly dau cach' trong string
    private static String fixSpacesString(String s) {
        return s.trim().replaceAll("\\s+", " ");
    }

    public List<Product> searchProductReturnListProduct(String query) {
        // Xoa table invertedIndex cu~ di, lam cai moi' (2 dong code nay chi phuc vu viec test)
        invertedIndexRepository.deleteAll();
        updateInverted(); // Cai nay chi dung de update lai danh sach, khi su dung that su thi ko can

        List<Product> foundProducts = new ArrayList<Product>();
        query = unAccent(fixSpacesString(query));
        String[] wordList = query.split(" ");

        // Danh sach cac word can kiem tra.
        List<InvertedIndex> keyList = new ArrayList<InvertedIndex>();
        for (String word : wordList) {
            InvertedIndex temp = invertedIndexRepository.findOne(word);
            if (temp != null) {
                keyList.add(temp);
            }
        }

        if (keyList.size() == 0) return null; // size() = 0 => ko tim thay tu khoa' nao hop le

        List<Integer> productIDlist = intersection(keyList);
        for (int productID : productIDlist) {
            foundProducts.add(productRepository.findOne(productID));
        }
        return foundProducts;
    }

    /*
    * Ham tim giao cua cac tap hop
    * */
    private List<Integer> intersection(List<InvertedIndex> keyList) {
        List<Integer> result = new ArrayList<Integer>();

        for (InvertedIndex key : keyList) {
            List<Integer> temp = convertStringToIntList(key.getSequence());
            if (result.size() == 0) result.addAll(temp);
            result.retainAll(temp);
        }
        return result;
    }

}
