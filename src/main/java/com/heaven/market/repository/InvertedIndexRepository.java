package com.heaven.market.repository;

import com.heaven.market.model.InvertedIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by son on 24/04/2016.
 */
@Repository
public interface InvertedIndexRepository extends CrudRepository<InvertedIndex, String> {
}
