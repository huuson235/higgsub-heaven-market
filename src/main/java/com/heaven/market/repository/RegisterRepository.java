package com.heaven.market.repository;

import com.heaven.market.model.Register;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Storm on 23/04/2016.
 */
@Repository
public interface RegisterRepository extends CrudRepository<Register, String>{
}
