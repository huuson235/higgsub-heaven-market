package com.heaven.market.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Storm on 23/04/2016.
 */
@Entity
public class Product {
    @Id
    @GeneratedValue
    @Column(unique = true, nullable = false)
    private int productId;

    private String nameProductVN; // Có dấu
    private String nameProductENG; // Không dấu
    private String content; // Nội dung quảng cáo
    private String imageProduct;
    private String priceProduct;
    private String typeProduct;
    @OneToMany
    private List<Comment> commentList = new ArrayList<Comment>();
    private boolean statusProduct; // False = not available, True = available

    public Product() {
        this.statusProduct = true;
    }

    public String getTypeProduct() {
        return typeProduct;
    }

    public void setTypeProduct(String typeProduct) {
        this.typeProduct = typeProduct;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getImageProduct() {
        return imageProduct;
    }

    public void setImageProduct(String imageProduct) {
        this.imageProduct = imageProduct;
    }

    public String getNameProductVN() {
        return nameProductVN;
    }

    public void setNameProductVN(String nameProductVN) {
        this.nameProductVN = nameProductVN;
    }

    public String getNameProductENG() {
        return nameProductENG;
    }

    public void setNameProductENG(String nameProductENG) {
        this.nameProductENG = nameProductENG;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPriceProduct() {
        return priceProduct;
    }

    public void setPriceProduct(String priceProduct) {
        this.priceProduct = priceProduct;
    }

    public boolean isStatusProduct() {
        return statusProduct;
    }

    public void setStatusProduct(boolean statusProduct) {
        this.statusProduct = statusProduct;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }
}
