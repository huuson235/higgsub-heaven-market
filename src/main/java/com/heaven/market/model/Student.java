package com.heaven.market.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.*;

/**
 * Created by Storm on 23/04/2016.
 */
@Entity
public class Student {
    @Id
    private String token;
    @Column(unique = true, nullable = false) // Son edited
    private String username;
    private String password;
    private String email;
    private String expiredDate;
    private String trustInfo;

    public Student(String password) {
        this.password = password;
    }

    @OneToMany
    List<Product> productList = new ArrayList<Product>();


    public Student(){
        this.password = "uet.edu.vn";
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getTrustInfo() {
        return trustInfo;
    }

    public void setTrustInfo(String trustInfo) {
        this.trustInfo = trustInfo;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
