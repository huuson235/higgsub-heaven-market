package com.heaven.market.controller;

import com.heaven.market.controller.dto.LoginResultDTO;
import com.heaven.market.controller.dto.StatusDTO;
import com.heaven.market.controller.dto.UserDTO;
import com.heaven.market.controller.stereotype.RequiredRoles;
import com.heaven.market.controller.stereotype.NoAuthentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {
    @NoAuthentication
    @RequestMapping("/login")
    public LoginResultDTO login() {
        return new LoginResultDTO();
    }

    @RequestMapping("/logout")
    public StatusDTO logout() {
        return new StatusDTO();
    }

    @RequestMapping("/createUser")
    public UserDTO createUser(UserDTO userDTO) {
        return new UserDTO();
    }

    @RequestMapping("/isGuest")
    public String isGuest() {
        return "Yes, guest is accessible";
    }

    @RequiredRoles("Register")
    @RequestMapping("/isRegistered")
    public String isRegistered() {
        return "Yes, registered user is accessible";
    }

    @RequiredRoles("Student")
    @RequestMapping("/student")
    public String isStudent() {
        return "Yes, registered user is accessible";
    }
}
