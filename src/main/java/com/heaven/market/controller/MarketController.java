package com.heaven.market.controller;

import com.heaven.market.model.Product;
import com.heaven.market.model.Student;
import com.heaven.market.service.HeavenMarketService;
import com.heaven.market.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Storm on 23/04/2016.
 */
@RestController
public class MarketController {
    @Autowired
    private HeavenMarketService heavenMarketService;
    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "/addStudent", method = RequestMethod.POST)
    public Student createStudent(@RequestBody Student student){
        return heavenMarketService.createStudent(student);
    }

    @RequestMapping(value = "/getStudent/{username}", method = RequestMethod.GET)
    public Student getStudent(@PathVariable("username") String username){
        return heavenMarketService.findStudentByUsername(username);
    }

    @RequestMapping(value = "/search/{username}", method = RequestMethod.GET)
    public List<Product> getProduct(@PathVariable("username") String nameProduct) {
        return heavenMarketService.findStudentByUsername(nameProduct).getProductList();
    }

    @RequestMapping(value = "/getStudent/{username}/addProduct", method = RequestMethod.POST)
    public Product addProduct(@RequestBody Product product, @PathVariable("username") String username){
        return heavenMarketService.createProduct(username, product);
    }

    @RequestMapping(value = "/searches/{nameProduct}", method = RequestMethod.GET)
    public List<Product> getProducts(@PathVariable("nameProduct") String nameProduct){
        return heavenMarketService.searchProduct(nameProduct);
    }

    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public List<Student> getStudents(){
        return heavenMarketService.getStudents();
    }

    /*Cod of Son */
    @RequestMapping(value = "/searchByInvertedIndex/{nameProduct}", method = RequestMethod.GET)
    public List<Product> getProductsByInvertedIndex(@PathVariable("nameProduct") String nameProduct){
        return searchService.searchProductReturnListProduct(nameProduct);
    }
}
