package com.heaven.market.interceptor;

import com.heaven.market.controller.stereotype.RequiredRoles;
import com.heaven.market.service.HeavenMarketService;
import com.heaven.market.controller.stereotype.NoAuthentication;
import com.heaven.market.exception.InterceptorException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by lent on 4/20/2016.
 */
@Component
public class TokenAuthenticationInterceptor extends HandlerInterceptorAdapter {

    private HeavenMarketService heavenMarketService;
    private String role;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception{
        String token = request.getHeader("auth-token");
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            if (handlerMethod.getMethodAnnotation(NoAuthentication.class) != null) {
                // No required authentication
                return true; // by pass
            } else if (token == null) {
                throw new InterceptorException("Required token", HttpStatus.UNAUTHORIZED); // By default required token
            }
            /*
            if(heavenMarketService.isStudent(token))
                role = "Student";
            if(heavenMarketService.isRegister(token))
                role = "Register";
            if(token.equals("admin"))
                role = "Administrator";
            */
            if(token.equals("admin"))
                role = "Student";
            RequiredRoles requiredRoles = handlerMethod.getMethodAnnotation(RequiredRoles.class);
            if (requiredRoles != null) {
                for (String requiredRole : requiredRoles.value()) {
                    if (requiredRole.equals(role)) {
                        return true;
                    }
                }
                throw new InterceptorException("No matching role found", HttpStatus.UNAUTHORIZED);
            }
        }
        return true;
    }
}
